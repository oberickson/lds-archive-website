Hot Chocolate
============

A static-file markdown blogging engine written in Ruby and powed by Sinatra. 

"Writing your own blog platform is like roasting your own coffee: it's impractical and you probably shouldn't do it, but for people who really, truly care about it, it's worthwhile to them for their own personal priorities that sound crazy to everyone else. Well, I write my own blog platform and I roast my own coffee." - Marco Arment

Installation Instructions
------------------------------

One of my main goals when developing Hot Chocolate is that it would be super easy to install locally on your computer and just as easy to deploy to your web server.

To start using Hot Chocolate to power your very own blog follow these steps:

1. Fork this repository
2. Clone your forked repository to your computer
3. Using the command line change directory to inside of the hot chocolate
4. Follow these [instructions](https://rvm.io/rvm/install/) to install ruby 
5. Run this command to install gems that hot chocolate requires: gem install sinatra slim rdiscount
6. Type: ruby hotchocolate.rb and your point your browser to http://localhost:4567 to see your new blog

### Deploy To Heroku
Hot Chocolate comes ready to deploy to heroku, so all you have to do is install the heroku toolbelt and type the following two commands

1. heroku create
2. git push heroku master

Now if you browse to the url listed in your terminal you can see your live website. It's really that easy.

Directory Structure
-------------------

Hot Chocolate is a static-file blogging engine so it depends on having a certain directory structure. If you forked the project all the right directories should already be in place, so I'm just going to explain them here:


